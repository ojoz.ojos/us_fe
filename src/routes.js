import React from 'react'

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'))
const CustomerList = React.lazy(() => import('./views/customer/list/List'))
const CustomerCreate = React.lazy(() => import('./views/customer/create/Create'))
const CustomerEdit = React.lazy(() => import('./views/customer/edit/Edit'))
const ProductList = React.lazy(() => import('./views/product/list/List'))
const ProductCreate = React.lazy(() => import('./views/product/create/Create'))
const ProductEdit = React.lazy(() => import('./views/product/edit/Edit'))
const TransactionList = React.lazy(() => import('./views/transaction/list/List'))
const TransactionCreate = React.lazy(() => import('./views/transaction/create/Create'))
const TransactionEdit = React.lazy(() => import('./views/transaction/edit/Edit'))

const routes = [
  { path: '/', exact: true, name: 'Home', element: Dashboard },
  { path: '/dashboard', name: 'Dashboard', element: Dashboard },
  { path: '/customer', name: 'Customer', element: CustomerList },
  { path: '/customer/create', name: 'Create', element: CustomerCreate },
  { path: '/customer/edit', name: 'Edit', element: CustomerEdit },
  { path: '/product', name: 'Product', element: ProductList },
  { path: '/product/create', name: 'Create', element: ProductCreate },
  { path: '/product/edit', name: 'Edit', element: ProductEdit },
  { path: '/transaction', name: 'Transaction', element: TransactionList },
  { path: '/transaction/create', name: 'Create', element: TransactionCreate },
  { path: '/transaction/edit', name: 'Edit', element: TransactionEdit },
]

export default routes
