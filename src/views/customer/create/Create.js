import React, { useState, useRef } from 'react'
import {
  CButton,
  CCol,
  CFormInput,
  CFormSelect,
  CRow,
  CToast,
  CToastBody,
  CToaster,
  CToastHeader,
} from '@coreui/react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

const Create = () => {
  const [name, setName] = useState('')
  const [gender, setGender] = useState('')
  const [domicile, setDomicile] = useState('')

  const navigate = useNavigate()

  const saveHandler = () => {
    saveData()
  }

  const saveData = async () => {
    const request = { name, gender, domicile }
    const instance = axios.create({
      baseURL: `${process.env.REACT_APP_API_URL}`,
      headers: { 'X-Authorization': process.env.REACT_APP_API_TOKEN },
    })
    await instance
      .post('/customers/create', request)
      .then((res) => {
        setName('')
        setGender('')
        setDomicile('')
        addToast(successToast('Success', 'Create new customer.'))
        setTimeout(() => {
          navigate('/customer')
        }, 2000)
      })
      .catch((error) => {
        console.error('Error', error)
      })
  }

  const [toast, addToast] = useState(0)
  const toaster = useRef()
  const successToast = (title, body) => {
    return (
      <CToast
        animation={true}
        autohide={true}
        color="success"
        className="text-white align-items-center"
      >
        <CToastHeader closeButton>
          <strong className="me-auto">{title}</strong>
        </CToastHeader>
        <CToastBody>{body}</CToastBody>
      </CToast>
    )
  }

  return (
    <>
      <CRow>
        <CCol xs={12}>
          <h2>Create New Customer</h2>
        </CCol>
        <CCol xs={12} className="mt-4">
          <div>
            <CFormInput
              type="text"
              id="nameInput"
              floatingLabel="Name"
              placeholder="Your name"
              onChange={(e) => setName(e.target.value)}
              value={name}
            />
          </div>
          <div className="mt-3">
            <CFormSelect
              id="genderSelect"
              floatingLabel="Gender"
              aria-label="Floating label select example"
              onChange={(e) => setGender(e.target.value)}
              value={gender}
            >
              <option>Open this select gender</option>
              <option value="PRIA">Pria</option>
              <option value="WANITA">Wanita</option>
            </CFormSelect>
          </div>
          <div className="mt-3">
            <CFormInput
              type="text"
              id="domicileInput"
              floatingLabel="Domicile"
              placeholder="Your Domicile"
              onChange={(e) => setDomicile(e.target.value)}
              value={domicile}
            />
          </div>
          <div className="mt-3">
            <CButton type="submit" color="info" onClick={saveHandler}>
              Submit
            </CButton>
          </div>
        </CCol>
      </CRow>
      <CToaster ref={toaster} push={toast} placement="top-end" />
    </>
  )
}

export default Create
