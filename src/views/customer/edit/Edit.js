import React, { useState, useEffect, useRef } from 'react'
import {
  CButton,
  CCol,
  CFormInput,
  CFormSelect,
  CRow,
  CToast,
  CToastBody,
  CToaster,
  CToastHeader,
} from '@coreui/react'
import axios from 'axios'
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css'
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css'
import { useNavigate, useSearchParams } from 'react-router-dom'

const ListCustomers = () => {
  const [name, setName] = useState('')
  const [gender, setGender] = useState('')
  const [domicile, setDomicile] = useState('')
  const [searchParams, setSearchParams] = useSearchParams()
  const customerId = searchParams.get('id')

  useEffect(() => {
    getCustomerData()
  }, [])

  const getCustomerData = () => {
    const instance = axios.create({
      baseURL: `${process.env.REACT_APP_API_URL}`,
      headers: { 'X-Authorization': process.env.REACT_APP_API_TOKEN },
    })
    instance
      .post(`/customers/show?id=${customerId}`)
      .then((res) => {
        const result = res.data.data[0]
        setName(result.name)
        setGender(result.gender)
        setDomicile(result.domicile)
      })
      .catch((error) => {
        console.error('Error', error)
      })
  }

  const navigate = useNavigate()

  const updateHandler = () => {
    updateData()
  }

  const updateData = async () => {
    const request = { id: customerId, name, gender, domicile }
    const instance = axios.create({
      baseURL: `${process.env.REACT_APP_API_URL}`,
      headers: { 'X-Authorization': process.env.REACT_APP_API_TOKEN },
    })
    await instance
      .put('/customers/update', request)
      .then((res) => {
        addToast(successToast('Success', 'Edit customer.'))
        setTimeout(() => {
          navigate('/customer')
        }, 2000)
      })
      .catch((error) => {
        console.error('Error', error)
      })
  }

  const [toast, addToast] = useState(0)
  const toaster = useRef()
  const successToast = (title, body) => {
    return (
      <CToast
        animation={true}
        autohide={true}
        color="success"
        className="text-white align-items-center"
      >
        <CToastHeader closeButton>
          <strong className="me-auto">{title}</strong>
        </CToastHeader>
        <CToastBody>{body}</CToastBody>
      </CToast>
    )
  }

  return (
    <>
      <CRow>
        <CCol xs={12}>
          <h2>Edit Customer</h2>
        </CCol>
        <CCol xs={12} className="mt-4">
          <div>
            <CFormInput
              type="text"
              id="nameInput"
              floatingLabel="Name"
              placeholder="Your name"
              onChange={(e) => setName(e.target.value)}
              value={name}
            />
          </div>
          <div className="mt-3">
            <CFormSelect
              id="genderSelect"
              floatingLabel="Gender"
              aria-label="Floating label select example"
              onChange={(e) => setGender(e.target.value)}
              value={gender}
            >
              <option>Open this select gender</option>
              <option value="PRIA">Pria</option>
              <option value="WANITA">Wanita</option>
            </CFormSelect>
          </div>
          <div className="mt-3">
            <CFormInput
              type="text"
              id="domicileInput"
              floatingLabel="Domicile"
              placeholder="Your Domicile"
              onChange={(e) => setDomicile(e.target.value)}
              value={domicile}
            />
          </div>
          <div className="mt-3">
            <CButton type="submit" color="info" onClick={updateHandler}>
              Submit
            </CButton>
          </div>
        </CCol>
      </CRow>
      <CToaster ref={toaster} push={toast} placement="top-end" />
    </>
  )
}

export default ListCustomers
