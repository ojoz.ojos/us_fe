import React, { useState, useEffect, useRef } from 'react'
import { CButton, CCol, CRow, CToast, CToastBody, CToaster, CToastHeader } from '@coreui/react'
import axios from 'axios'
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css'
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css'
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from 'react-bootstrap-table2-paginator'
import { Link, useNavigate } from 'react-router-dom'
import CIcon from '@coreui/icons-react'
import { cilList, cilTrash } from '@coreui/icons'

const ListCustomers = () => {
  const [customers, setCustomers] = useState([])
  const navigate = useNavigate()

  useEffect(() => {
    getCustomerData()
  }, [])

  const getCustomerData = () => {
    const instance = axios.create({
      baseURL: `${process.env.REACT_APP_API_URL}`,
      headers: { 'X-Authorization': process.env.REACT_APP_API_TOKEN },
    })
    instance
      .post('/customers/showAll?limit=1000')
      .then((res) => {
        setCustomers(res.data.data)
      })
      .catch((error) => {
        console.error('Error', error)
      })
  }

  const removeHandler = (id) => {
    removeCustomer(id)
  }

  const removeCustomer = async (id) => {
    const instance = axios.create({
      baseURL: `${process.env.REACT_APP_API_URL}`,
      headers: { 'X-Authorization': process.env.REACT_APP_API_TOKEN },
    })
    await instance
      .delete(`/customers/delete?id=${id}`)
      .then((res) => {
        getCustomerData()
        addToast(successToast('Success', 'Remove customer'))
      })
      .catch((error) => {
        console.error('Error', error)
      })
  }

  const editHandler = (id) => {
    navigate(`/customer/edit?id=${id}`)
  }

  const [toast, addToast] = useState(0)
  const toaster = useRef()
  const successToast = (title, body) => {
    return (
      <CToast
        animation={true}
        autohide={true}
        color="success"
        className="text-white align-items-center"
      >
        <CToastHeader closeButton>
          <strong className="me-auto">{title}</strong>
        </CToastHeader>
        <CToastBody>{body}</CToastBody>
      </CToast>
    )
  }

  const columns = [
    {
      dataField: 'name',
      text: 'Name',
      sort: true,
    },
    {
      dataField: 'gender',
      text: 'Gender',
      sort: true,
    },
    {
      dataField: 'domicile',
      text: 'Domicile',
      sort: true,
    },
    {
      dataField: 'aa',
      text: 'Action',
      formatter: (cell, row) => {
        const id = row.id
        return (
          <>
            <CButton title="Edit" type="button" color="warning" onClick={() => editHandler(id)}>
              <CIcon icon={cilList} size="lg" />
            </CButton>
            <CButton
              title="Remove"
              type="button"
              color="danger"
              style={{ marginLeft: '6px' }}
              onClick={() => removeHandler(id)}
            >
              <CIcon icon={cilTrash} size="lg" />
            </CButton>
          </>
        )
      },
    },
  ]

  const options = {
    sizePerPageList: [
      {
        text: '5',
        value: 5,
      },
      {
        text: '10',
        value: 10,
      },
      {
        text: 'All',
        value: customers.length,
      },
    ],
  }

  return (
    <>
      <CRow>
        <CCol xs={12}>
          <div className="d-flex justify-content-between">
            <h2 className="m-0">Customer</h2>
            <Link to="/customer/create" className="btn btn-success">
              Add
            </Link>
          </div>
        </CCol>
        <CCol xs={12} className="mt-4">
          <BootstrapTable
            keyField="id"
            data={customers}
            columns={columns}
            pagination={paginationFactory(options)}
            hover
          />
        </CCol>
      </CRow>
      <CToaster ref={toaster} push={toast} placement="top-end" />
    </>
  )
}

export default ListCustomers
