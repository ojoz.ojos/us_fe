import React, { useState, useEffect, useRef } from 'react'
import { CButton, CCol, CRow, CToast, CToastBody, CToaster, CToastHeader } from '@coreui/react'
import axios from 'axios'
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css'
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css'
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from 'react-bootstrap-table2-paginator'
import { Link, useNavigate } from 'react-router-dom'
import CIcon from '@coreui/icons-react'
import { cilList, cilTrash } from '@coreui/icons'

const ListProducts = () => {
  const [products, setProducts] = useState([])
  const navigate = useNavigate()

  useEffect(() => {
    getProductData()
  }, [])

  const getProductData = () => {
    const instance = axios.create({
      baseURL: `${process.env.REACT_APP_API_URL}`,
      headers: { 'X-Authorization': process.env.REACT_APP_API_TOKEN },
    })
    instance
      .post('/products/showAll?limit=1000')
      .then((res) => {
        setProducts(res.data.data)
      })
      .catch((error) => {
        console.error('Error', error)
      })
  }

  const removeHandler = (id) => {
    removeProduct(id)
  }

  const removeProduct = async (id) => {
    const instance = axios.create({
      baseURL: `${process.env.REACT_APP_API_URL}`,
      headers: { 'X-Authorization': process.env.REACT_APP_API_TOKEN },
    })
    await instance
      .delete(`/products/delete?id=${id}`)
      .then((res) => {
        getProductData()
        addToast(successToast('Success', 'Remove product'))
      })
      .catch((error) => {
        console.error('Error', error)
      })
  }

  const editHandler = (id) => {
    navigate(`/product/edit?id=${id}`)
  }

  const [toast, addToast] = useState(0)
  const toaster = useRef()
  const successToast = (title, body) => {
    return (
      <CToast
        animation={true}
        autohide={true}
        color="success"
        className="text-white align-items-center"
      >
        <CToastHeader closeButton>
          <strong className="me-auto">{title}</strong>
        </CToastHeader>
        <CToastBody>{body}</CToastBody>
      </CToast>
    )
  }

  const columns = [
    {
      dataField: 'name',
      text: 'Name',
      sort: true,
    },
    {
      dataField: 'category',
      text: 'Category',
      sort: true,
    },
    {
      dataField: 'price',
      text: 'Price',
      sort: true,
    },
    {
      dataField: 'aa',
      text: 'Action',
      formatter: (cell, row) => {
        const id = row.id
        return (
          <>
            <CButton title="Edit" type="button" color="warning" onClick={() => editHandler(id)}>
              <CIcon icon={cilList} size="lg" />
            </CButton>
            <CButton
              title="Remove"
              type="button"
              color="danger"
              style={{ marginLeft: '6px' }}
              onClick={() => removeHandler(id)}
            >
              <CIcon icon={cilTrash} size="lg" />
            </CButton>
          </>
        )
      },
    },
  ]

  const options = {
    sizePerPageList: [
      {
        text: '5',
        value: 5,
      },
      {
        text: '10',
        value: 10,
      },
      {
        text: 'All',
        value: products.length,
      },
    ],
  }

  return (
    <>
      <CRow>
        <CCol xs={12}>
          <div className="d-flex justify-content-between">
            <h2 className="m-0">Product</h2>
            <Link to="/product/create" className="btn btn-success">
              Add
            </Link>
          </div>
        </CCol>
        <CCol xs={12} className="mt-4">
          <BootstrapTable
            keyField="id"
            data={products}
            columns={columns}
            pagination={paginationFactory(options)}
            hover
          />
        </CCol>
      </CRow>
      <CToaster ref={toaster} push={toast} placement="top-end" />
    </>
  )
}

export default ListProducts
