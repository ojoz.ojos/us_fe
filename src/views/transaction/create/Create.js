import React, { useState, useRef, useEffect } from 'react'
import {
  CButton,
  CCol,
  CFormInput,
  CFormLabel,
  CRow,
  CToast,
  CToastBody,
  CToaster,
  CToastHeader,
} from '@coreui/react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import Select from 'react-select'

const Create = () => {
  const [customers, setCustomers] = useState([])
  const [products, setProducts] = useState([])

  const [date, setDate] = useState(new Date().toISOString().substring(0, 19))
  const [customer, setCustomer] = useState([])
  const [formFields, setFormFields] = useState([{ product: '', quantity: '' }])

  useEffect(() => {
    getCustomerData()
    getProductData()
  }, [])

  const getCustomerData = () => {
    const instance = axios.create({
      baseURL: `${process.env.REACT_APP_API_URL}`,
      headers: { 'X-Authorization': process.env.REACT_APP_API_TOKEN },
    })
    instance
      .post('/customers/showAll?limit=1000')
      .then((res) => {
        const customers = res.data.data.map((x) => {
          return { value: x.id, label: x.name }
        })
        setCustomers(customers)
      })
      .catch((error) => {
        console.error('Error', error)
      })
  }

  const getProductData = () => {
    const instance = axios.create({
      baseURL: `${process.env.REACT_APP_API_URL}`,
      headers: { 'X-Authorization': process.env.REACT_APP_API_TOKEN },
    })
    instance
      .post('/products/showAll?limit=1000')
      .then((res) => {
        const products = res.data.data.map((x) => {
          return { value: x.id, label: x.name }
        })
        setProducts(products)
      })
      .catch((error) => {
        console.error('Error', error)
      })
  }

  const navigate = useNavigate()

  const saveHandler = () => {
    saveData()
  }

  const saveData = async () => {
    const request = {
      date: date.replace('T', ' '),
      customer,
      products: formFields,
    }

    const instance = axios.create({
      baseURL: `${process.env.REACT_APP_API_URL}`,
      headers: {
        'X-Authorization': process.env.REACT_APP_API_TOKEN,
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
      },
    })
    await instance
      .post('/transactions/create', request)
      .then((res) => {
        addToast(successToast('Success', 'Create new transaction.'))
        setTimeout(() => {
          navigate('/transaction')
        }, 2000)
      })
      .catch((error) => {
        console.error('Error', error)
      })
  }

  const [toast, addToast] = useState(0)
  const toaster = useRef()
  const successToast = (title, body) => {
    return (
      <CToast
        animation={true}
        autohide={true}
        color="success"
        className="text-white align-items-center"
      >
        <CToastHeader closeButton>
          <strong className="me-auto">{title}</strong>
        </CToastHeader>
        <CToastBody>{body}</CToastBody>
      </CToast>
    )
  }

  const handleFormChange = (event, index) => {
    let data = [...formFields]
    data[index][event.target.name] = isNaN(parseInt(event.target.value))
      ? ''
      : parseInt(event.target.value)
    setFormFields(data)
  }

  const handleSelectChange = (event, index, name) => {
    if (event.value) {
      let data = [...formFields]
      data[index][name] = event.value
      setFormFields(data)
    }
  }

  const addFields = () => {
    let object = {
      product: '',
      quantity: '',
    }

    setFormFields([...formFields, object])
  }

  const removeFields = (index) => {
    let data = [...formFields]
    data.splice(index, 1)
    setFormFields(data)
  }

  return (
    <>
      <CRow>
        <CCol xs={12}>
          <h2>Create New Transaction</h2>
        </CCol>
        <CCol xs={12} className="mt-4">
          <div>
            <CFormLabel>Date</CFormLabel>
            <CFormInput
              name="date"
              type="datetime-local"
              placeholder="Date"
              onChange={(event) => setDate(event.target.value)}
              value={date}
            />
          </div>
          <div className="mt-3">
            <CFormLabel>Customer</CFormLabel>
            <Select
              placeholder="Choose one"
              options={customers}
              onChange={(event) => setCustomer(event.value)}
            />
          </div>
          <div className="mt-3">
            <CFormLabel>Product</CFormLabel>
            {formFields.map((form, index) => {
              return (
                <div key={index} className="d-flex mb-2">
                  <div style={{ width: '100%', maxWidth: '600px' }}>
                    <Select
                      name="product"
                      placeholder="Product"
                      onChange={(event) => handleSelectChange(event, index, 'product')}
                      options={products}
                      isOptionDisabled={(option) => option.disabled}
                    />
                  </div>
                  <div style={{ marginLeft: '5px' }}>
                    <CFormInput
                      name="quantity"
                      placeholder="Quantity"
                      onChange={(event) => handleFormChange(event, index)}
                      value={form.quantity}
                      style={{ width: '100px' }}
                    />
                  </div>
                  <div style={{ marginLeft: '5px' }}>
                    <CButton color={'danger'} onClick={() => removeFields(index)}>
                      &times;
                    </CButton>
                  </div>
                </div>
              )
            })}
            <div className="mt-3">
              <CButton onClick={addFields}>Add Product</CButton>
            </div>
          </div>
          <div className="mt-3">
            <CButton type="submit" color="info" onClick={saveHandler}>
              Submit
            </CButton>
          </div>
        </CCol>
      </CRow>
      <CToaster ref={toaster} push={toast} placement="top-end" />
    </>
  )
}

export default Create
