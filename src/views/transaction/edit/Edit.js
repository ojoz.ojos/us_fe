import React, { useState, useEffect, useRef } from 'react'
import {
  CButton,
  CCol,
  CFormInput,
  CRow,
  CToast,
  CToastBody,
  CToaster,
  CToastHeader,
} from '@coreui/react'
import axios from 'axios'
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css'
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css'
import { useNavigate, useSearchParams } from 'react-router-dom'

const ListProducts = () => {
  const [name, setName] = useState('')
  const [category, setCategory] = useState('')
  const [price, setPrice] = useState('')
  const [searchParams] = useSearchParams()
  const productId = searchParams.get('id')

  useEffect(() => {
    getProductData()
  }, [])

  const getProductData = () => {
    const instance = axios.create({
      baseURL: `${process.env.REACT_APP_API_URL}`,
      headers: { 'X-Authorization': process.env.REACT_APP_API_TOKEN },
    })
    instance
      .post(`/products/show?id=${productId}`)
      .then((res) => {
        const result = res.data.data[0]
        setName(result.name)
        setCategory(result.category)
        setPrice(result.price)
      })
      .catch((error) => {
        console.error('Error', error)
      })
  }

  const navigate = useNavigate()

  const updateHandler = () => {
    updateData()
  }

  const updateData = async () => {
    const request = { id: productId, name, category, price }
    const instance = axios.create({
      baseURL: `${process.env.REACT_APP_API_URL}`,
      headers: { 'X-Authorization': process.env.REACT_APP_API_TOKEN },
    })
    await instance
      .put('/products/update', request)
      .then((res) => {
        addToast(successToast('Success', 'Edit product.'))
        setTimeout(() => {
          navigate('/product')
        }, 2000)
      })
      .catch((error) => {
        console.error('Error', error)
      })
  }

  const [toast, addToast] = useState(0)
  const toaster = useRef()
  const successToast = (title, body) => {
    return (
      <CToast
        animation={true}
        autohide={true}
        color="success"
        className="text-white align-items-center"
      >
        <CToastHeader closeButton>
          <strong className="me-auto">{title}</strong>
        </CToastHeader>
        <CToastBody>{body}</CToastBody>
      </CToast>
    )
  }

  return (
    <>
      <CRow>
        <CCol xs={12}>
          <h2>Edit Transaction</h2>
        </CCol>
        <CCol xs={12} className="mt-4">
          <div>
            <CFormInput
              type="text"
              id="nameInput"
              floatingLabel="Name"
              placeholder="Your name"
              onChange={(e) => setName(e.target.value)}
              value={name}
            />
          </div>
          <div className="mt-3">
            <CFormInput
              type="text"
              id="categoryInput"
              floatingLabel="Category"
              placeholder="Category"
              onChange={(e) => setCategory(e.target.value)}
              value={category}
            />
          </div>
          <div className="mt-3">
            <CFormInput
              type="text"
              id="priceInput"
              floatingLabel="Price"
              placeholder="Your Price"
              onChange={(e) => setPrice(e.target.value)}
              value={price}
            />
          </div>
          <div className="mt-3">
            <CButton type="submit" color="info" onClick={updateHandler}>
              Submit
            </CButton>
          </div>
        </CCol>
      </CRow>
      <CToaster ref={toaster} push={toast} placement="top-end" />
    </>
  )
}

export default ListProducts
