import React, { useState, useEffect, useRef } from 'react'
import {
  CButton,
  CCol,
  CFormInput,
  CFormLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
  CToast,
  CToastBody,
  CToaster,
  CToastHeader,
} from '@coreui/react'
import axios from 'axios'
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css'
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css'
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from 'react-bootstrap-table2-paginator'
import { Link, useNavigate } from 'react-router-dom'
import CIcon from '@coreui/icons-react'
import { cilList, cilTrash } from '@coreui/icons'
import { cilFile } from '@coreui/icons/js/free'

const ListTransactions = () => {
  const [transactions, setTransactions] = useState([])
  const navigate = useNavigate()
  const [visible, setVisible] = useState(false)

  const [selectedDetail, setSelectedDetail] = useState([])

  useEffect(() => {
    getTransactionData()
  }, [])

  const getTransactionData = () => {
    const instance = axios.create({
      baseURL: `${process.env.REACT_APP_API_URL}`,
      headers: { 'X-Authorization': process.env.REACT_APP_API_TOKEN },
    })
    instance
      .post('/transactions/showAll?limit=1000')
      .then((res) => {
        setTransactions(res.data.data)
      })
      .catch((error) => {
        console.error('Error', error)
      })
  }

  const removeHandler = (id) => {
    removeTransaction(id)
  }

  const removeTransaction = async (id) => {
    const instance = axios.create({
      baseURL: `${process.env.REACT_APP_API_URL}`,
      headers: { 'X-Authorization': process.env.REACT_APP_API_TOKEN },
    })
    await instance
      .delete(`/transactions/delete?id=${id}`)
      .then((res) => {
        getTransactionData()
        addToast(successToast('Success', 'Remove transaction'))
      })
      .catch((error) => {
        console.error('Error', error)
      })
  }

  const editHandler = (id) => {
    navigate(`/transaction/edit?id=${id}`)
  }

  function detailHandler(id) {
    setVisible(!visible)

    const selectedData = transactions.filter((x) => x.id === id)
    setSelectedDetail(selectedData[0])
  }

  const [toast, addToast] = useState(0)
  const toaster = useRef()
  const successToast = (title, body) => {
    return (
      <CToast
        animation={true}
        autohide={true}
        color="success"
        className="text-white align-items-center"
      >
        <CToastHeader closeButton>
          <strong className="me-auto">{title}</strong>
        </CToastHeader>
        <CToastBody>{body}</CToastBody>
      </CToast>
    )
  }

  const columns = [
    {
      dataField: 'date',
      text: 'Date',
      sort: true,
    },
    {
      dataField: 'customer.name',
      text: 'Customer',
      sort: true,
    },
    {
      dataField: 'subtotal',
      text: 'Subtotal',
      sort: true,
    },
    {
      dataField: 'aa',
      text: 'Action',
      formatter: (cell, row) => {
        const id = row.id
        return (
          <>
            <CButton
              title="Detail"
              type="button"
              color="secondary"
              onClick={() => detailHandler(id)}
            >
              <CIcon icon={cilFile} size="lg" />
            </CButton>
            <CButton
              title="Edit"
              type="button"
              color="warning"
              style={{ marginLeft: '6px' }}
              onClick={() => editHandler(id)}
            >
              <CIcon icon={cilList} size="lg" />
            </CButton>
            <CButton
              title="Remove"
              type="button"
              color="danger"
              style={{ marginLeft: '6px' }}
              onClick={() => removeHandler(id)}
            >
              <CIcon icon={cilTrash} size="lg" />
            </CButton>
          </>
        )
      },
    },
  ]

  const options = {
    sizePerPageList: [
      {
        text: '5',
        value: 5,
      },
      {
        text: '10',
        value: 10,
      },
      {
        text: 'All',
        value: transactions.length,
      },
    ],
  }

  return (
    <>
      <CRow>
        <CCol xs={12}>
          <div className="d-flex justify-content-between">
            <h2 className="m-0">Transaction</h2>
            <Link to="/transaction/create" className="btn btn-success">
              Add
            </Link>
          </div>
        </CCol>
        <CCol xs={12} className="mt-4">
          <BootstrapTable
            keyField="id"
            data={transactions}
            columns={columns}
            pagination={paginationFactory(options)}
            hover
          />
        </CCol>
      </CRow>

      <CModal alignment="center" size="xl" visible={visible} onClose={() => setVisible(false)}>
        <CModalHeader>
          <CModalTitle>Detail Transaction</CModalTitle>
        </CModalHeader>
        <CModalBody>
          {typeof selectedDetail.date !== 'undefined' ? (
            <div>
              <div>
                <h5>Order</h5>
              </div>
              <div className="d-flex justify-content-between">
                <div>
                  <div>Date</div>
                  <div>{selectedDetail.date}</div>
                </div>
                <div>
                  <div>Customer Name</div>
                  <div>{selectedDetail.customer.name}</div>
                </div>
                <div style={{ width: '150px' }}>
                  <div>Price</div>
                  <div>{selectedDetail.subtotal}</div>
                </div>
              </div>
              <div className="mt-4">
                <h5>Product</h5>
              </div>
              <div>
                {selectedDetail.products.map((p, i) => (
                  <div key={p.id} className="d-flex mt-2">
                    <div
                      className="text-center align-self-center"
                      style={{ minWidth: '20px', backgroundColor: '#e3e3e3' }}
                    >
                      {i + 1}
                    </div>
                    <div style={{ marginLeft: '10px' }}>
                      <div>
                        <span>Name: </span>
                        <span>{p.name}</span>
                      </div>
                      <div>
                        <span>Category: </span>
                        <span>{p.category}</span>
                      </div>
                      <div>
                        <span>Price: </span>
                        <span>{p.price}</span>
                      </div>
                      <div>
                        <span>Quantity: </span>
                        <span>{p.quantity}</span>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          ) : (
            ''
          )}
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisible(false)}>
            Close
          </CButton>
        </CModalFooter>
      </CModal>

      <CToaster ref={toaster} push={toast} placement="top-end" />
    </>
  )
}

export default ListTransactions
